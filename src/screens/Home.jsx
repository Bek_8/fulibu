import React, { useContext, useState } from "react";
import About from "../components/About";
import AboutCircle from "../components/AboutCircle";
import Content from "../components/Content";
import MyContext from "../context/MyContext";

export const Home = () => {
  const { language, apiService , lang, setLang } = useContext(MyContext);
  
  return (
    <>
      {/* {language[1].name} */}
      <AboutCircle/>      
      <main role="main" className="content">
        <About />
        {/* <button className="uz" onClick={() => {
          setLang(0)
        }} >uz</button> */}
        <Content />
      </main>
      
    </>
  );
};
