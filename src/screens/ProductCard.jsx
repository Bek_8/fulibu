import React from "react";
import ProductMainFirst from "../components/ProductCard/ProductMainFirst";
import ProductMainSecond from "../components/ProductCard/ProductMainSecond";
import Recommend from "../components/Recommend";
import Footer from "../components/Footer";
import { useEffect } from "react";
import { useContext } from "react";
import MyContext from "../context/MyContext";
import { useState } from "react";

export const ProductCard = ({ match }) => {
  const { apiService, token } = useContext(MyContext);
  const book_id = parseInt(match.params.id);
  const [book, setBook] = useState({});
  const [format, setFormat] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    apiService
      .getResources(`/book/${book_id}`, token)
      .then((value) => {
        if (value.statusCode === 200) {
          setBook(value.data);
          if(value.data.is_bought) {
            apiService.getResources("/extension/book/" + book_id, token).then(value => {
              if(value.statusCode === 200) {
                console.log(value.data);
                setFormat(value.data)
              }
            })
          }
          setLoading(false);
        }
      })
      .catch((e) => setLoading(false));
  }, []);
  if (loading) return <h1>Loading...</h1>;
  return (
    <>
      <main>
        <div className="container">
          <ProductMainFirst book={book} />
          <div className="main_second">Описание книги</div>
          <ProductMainSecond />
          {/* <div className="recommend">Рекомендации</div>
          <Recommend book={book} /> */}
        </div>
      </main>
      
    </>
  );
};
