import React from "react";
import ProductMainFirst from "../components/ProductCard/ProductMainFirst";
import ProductMainSecond from "../components/ProductCard/ProductMainSecond";
import Recommend from "../components/Recommend";
import Footer from "../components/Footer";
import { useEffect } from "react";
import { useContext } from "react";
import MyContext from "../context/MyContext";
import { useState } from "react";

export const CollectionCard = ({ match }) => {
  const { apiService } = useContext(MyContext);
  const book_id = parseInt(match.params.id);
  const [book, setBook] = useState({});
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    apiService
      .getResources(`/collection/${book_id}`)
      .then((value) => {
        if (value.statusCode === 200) {
          setBook(value.data);
          setLoading(false);
        }
      })
      .catch((e) => setLoading(false));
  }, []);
  if (loading) return <h1>Loading...</h1>;
  return (
    <>
      <main>
        <div className="container">
          <ProductMainFirst book={book} />
          <div className="main_second">Описание книги</div>
          <ProductMainSecond />
          <div className="recommend">Рекомендации</div>
          <Recommend />
        </div>
      </main>
    </>
  );
};
