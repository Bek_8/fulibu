import React from "react";
import ProfileHeader from "../components/profile/ProfileHeader";
import ProfileMain from "../components/profile/ProfileMain";
import Footer from "../components/Footer";
import Recommend from "../components/Recommend";

export const Profile = () => {
  return (
    <>
      <main className="container">
        <ProfileHeader />
        <ProfileMain />
        <Recommend />
      </main>
   
    </>
  );
};
