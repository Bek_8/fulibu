import React, { useEffect, useContext, useState } from "react";
import MyContext from "../context/MyContext";
import Searching from "../components/Search/Searching";
import Recommend from "../components/Recommend";
import Footer from "../components/Footer";
import Loader from "../components/Loader";
import Book from "../components/Book";

export const SearchPage = () => {
  const { apiService } = useContext(MyContext);
  const [loading, setLoading] = useState(true);
  const [recommend, setRecommend] = useState([]);
  const search = (e) => {
    let body = {};
    if(e){
      e.preventDefault();
      // const data = Object.fromEntries(new FormData(e.target).entries());
      let {elements} = e.target;
      const {keyword , category_id , type, date, rating} = elements;
      body = {
        ...(keyword.value.length > 0 && {keyword: keyword.value}),
        ...(parseInt(category_id.value) > 0 && {category_id: category_id.value}),
        ...(type.value) > 0 && {type: type.value},
        // date: date.value,
      }      
    } 
    if(!loading) setLoading(true);
    apiService
      .getResources("/search?" + new URLSearchParams(body))
      .then((value) => {
        console.log(value.data)
        setRecommend(value.data);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  }
  useEffect(() => {
    search()
  }, []);


  return (
    <>
      {/* {loading && <Loader/>} */}
      <div className="container">
        <div className="title">НАЙДИ СЕБЕ КНИГУ ПО ДУШЕ</div>
        <main>
          <form onSubmit={(e) => search(e)}>
          <Searching />
          </form>
          <div className="result">Результаты</div>
      {loading ? <Loader/> :   <div className="d-flex">
          {recommend.map((book,key) => <Book book={book} key={key} />)}
          </div>}
        </main>
      </div>
      <footer>
        <Footer />
      </footer>
    </>
  );
};
