import React, { useEffect, useContext, useState } from "react";
import MyContext from "../context/MyContext";
import Loader from "../components/Loader";
import { Link } from "react-router-dom";

export const RegisterLink = () => {
  const { apiService, token } = useContext(MyContext);
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    apiService
      .getResources("/user", token)
      .then((value) => {
        setUser(value);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  }, []);
  if (loading) return <Loader />;
  console.log(user);

  return (
    <>
      <div className="register">
        <div className="register_first">
          <div className="register_first_image">
            <img src={require("../images/reglink.svg")} alt="" />
          </div>
          <div className="register_first_welcome">Добро пожаловать</div>
          <div className="register_first_name">{user.name}!</div>
          <div className="register_second">
            <div className="register_second_text">
              Распространте ссылку своим друзьям и близким и получи шанс
              выйграть
            </div>
            <div className="register_first_spark">SPARK!</div>
            <div className="register_second_input">
              <button>fulibu.uz/{user.uuid}</button>
            </div>
            <Link to="/register-accept">
              <button className="register_second_btn">Скопировать</button>
            </Link>
          </div>
          <div className="register_third">Не сейчас</div>
        </div>
      </div>
    </>
  );
};
