const language = [
  //uz start
  { name: "laziz", recommend: "Tavsiyalar", more: "Koproq Kitoblar", hot: "Top Kitoblar", collection: "Toplam", new: "Yangilari", notRead: "Siz hali oqimagan kitoblar"  },
  //uz end
  //ru start
  { name: "Лазиз", recommend: "Рекомендации", more:"Найти больше книг", hot: "Хиты продаж", collection: "Сборники", new: "Новинки", notRead: "КНИГИ КОТОРЫЕ ТЫ ЕЩЕ НЕ ЧИТАЛ" },
  //ru end
  // //english start
  // { name: "Unberto", recommend: "Recommend" },
  // //english end
];
export default language;
