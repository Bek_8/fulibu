import React, { useEffect, useState } from "react";
import { Home } from "./screens/Home.jsx";
import { Switch, BrowserRouter, Route } from "react-router-dom";
import { ProductCard } from "./screens/ProductCard";
import { SearchPage } from "./screens/SearchPage.jsx";
import MyContext from "./context/MyContext.js";
import ApiService from "./api/index";
import language from "./language/index";
import { Profile } from "./screens/Profile.jsx";
import Nav from "../src/components/Nav";
import { Register } from "./screens/Register";
import { RegisterSecond } from "./screens/RegisterSecond";
import { RegisterThird } from "./screens/RegisterThird";
import { RegisterLink } from "./screens/RegisterLink";
import Loader from "./components/Loader";
import Footer from "./components/Footer.js";

const apiService = new ApiService();
function App() {
  const [token, setToken] = useState("");
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const [phone, setPhone] = useState(true);
  const [lang, setLang] = useState(1); /// 0 - uz 1 - ru 2 - en
  const getUserInfo = () => {
    const getter = localStorage.getItem("token");
    if (getter) {
      setToken(getter);
    }
    const phoneGet = localStorage.getItem("phone");
    if (phoneGet) {
      setPhone(phoneGet);
    }
    setLoading(false);
  };
  const setUserPhone = (value) => {
    setPhone(value);
    localStorage.setItem("phone", value);
  };
  const setUserToken = (value) => {
    setToken(value);
    localStorage.setItem("token", value);
  };

  useEffect(() => {
    getUserInfo();
  }, []);
  if (loading) return <Loader />;
  return (
    <MyContext.Provider
      value={{
        token,
        user,
        setUser,
        apiService,
        lang,
        setLang,
        language,
        phone,
        setUserPhone,
        setUserToken,
      }}
    >
      <BrowserRouter>
        <header className="header">
          <Nav />
        </header>
        {/* <div style={{ height: 200 }}></div> */}
        <Switch>
          <Route path={"/"} exact component={Home} />
          <Route path={"/product/:id"} component={ProductCard} />
          <Route path={"/collection/:id"} component={ProductCard} />
          <Route path={"/search-page"} component={SearchPage} />
          <Route path={"/profile"} component={Profile} />
          <Route path={"/auth"} component={Register} />
          <Route path={"/confirm"} component={RegisterSecond} />
          <Route path={"/register"} component={RegisterThird} />
          <Route path={"/referal-link"} component={RegisterLink} />
        </Switch>
        <footer>
          <Footer />
        </footer>
      </BrowserRouter>
    </MyContext.Provider>
  );
}

export default App;
