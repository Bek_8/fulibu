import { format } from "path";
import React, { useContext, useEffect } from "react";
import MyContext from "../../context/MyContext";

const ProductMainFirst = ({ book, history }) => {
  const {token, apiService} = useContext(MyContext)
  const purchaseOrder = () => {
    if(!token) {
      history.push("/auth")
    }
    else{
      const body = {
        item_id : book.id
      }
      apiService.postData("/checkout", token, body).then(value => {
        if(value.statusCode === 200){
          console.log(value);
        }
      })
    }
  }

  

  return (
    <>
      <div className="product">
        <div className="product_left">
          <div className="product_left_item">
            <img className="product_left_image" src={book.image[0]} alt="" />
          </div>
        </div>
        <div className="product_right">
          <div className="product_right_title">{book.name}</div>
          <div className="product_right_subtitle">
            {book.description}
          </div>
          <div className="product_right_description">
            <div className="product_right_subdescription">
              <div className="bold">Автор:</div>
              <div className="small">{book.author && book.author.name}</div>
            </div>
            <div className="product_right_subdescription">
              <div className="bold">Рейтинг:</div>
              <div className="image">
                <img src={require("../../images/star.png")} alt="" />
              </div>
              <div className="bold">4</div>
            </div>
            <div className="product_right_subdescription">
              <div className="bold">Дата выхода:</div>
              <div className="small">{book.publish_date}</div>
            </div>
            <div className="product_right_subdescription">
              <div className="bold">Объем:</div>
              <div className="small">{book.size} стр.</div>
            </div>
            <div className="product_right_subdescription">
              <div className="bold">Жанр:</div>
              <div className="small_color">
                {book.categories && book.categories.map((cat, i) => (
                (i < book.categories.length) ? <span key={cat.id}>{cat.name_ru + ", "}</span> : <span key={cat.id}>{cat.name_ru}</span>
                ))}
              </div>
            </div>
            {format && format.length > 0 &&
              (<div className="product_right_subdescription">
              <div className="bold">Форматы для скачивания:</div>
              <div className="small">{book.types && book.types.name}</div>
            </div>)}
          </div>
        </div>
      </div>
      <div className="bottom">
        <div className="bottom_price">
          <div className="bottom_price_logo">
            <img src={require("../../images/pricelogo.svg")} alt="" />
          </div>
          <div className="bottom_price_text">{book.price}</div>
        </div>
        <div className="bottom_library">
          <div className="bottom_library_logo">
            <img src={require("../../images/librarylogo.svg")} alt="" />
          </div>
          <div className="bottom_price_text">Добавить в библиотеку</div>
        </div>
        <button className="bottom_buy" onClick={purchaseOrder}>
          <div className="bottom_buy_logo">
            <img src={require("../../images/buylogo.svg")} alt="" />
          </div>
          <div className="bottom_buy_text">Купить</div>
        </button>
      </div>
    </>
  );
};

export default ProductMainFirst;
