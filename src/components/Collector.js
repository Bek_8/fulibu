import React from "react";
import InsideCollector from "./InsideCollector";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.scss";
import SwiperCore, { Autoplay } from "swiper";

SwiperCore.use([Autoplay]);

const Collector = ({ collectorBooks }) => {
  return (
    <>
      <Swiper
        loop={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        className="wrap collector-wrap"
        slidesPerView={4}
        breakpoints={{
          1200: {
            slidesPerView: 3,
          },
          992: {
            slidesPerView: 2,
          },
          768: {
            slidesPerView: 2,
          },
          600: {
            slidesPerView: 1,
          },
          400: {
            slidesPerView: 1,
          },
        }}
      >
        {collectorBooks &&
          collectorBooks.map((collectorBook, index) => {
            return (
              <SwiperSlide key={`${index} ${collectorBook.id}`}>
                <InsideCollector collectorBook={collectorBook} />
              </SwiperSlide>
            );
          })}
      </Swiper>
    </>
  );
};

export default Collector;
