import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.scss";

export default () => {
  return (
    <Swiper spaceBetween={50} slidesPerView={3}>
      <SwiperSlide></SwiperSlide>
    </Swiper>
  );
};
