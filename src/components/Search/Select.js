import React from "react";

export default function Select({ name, data }) {
  return (
    <label>
      <select name={name} required className="select">
        {data &&
          data.map((item, key) => {
            return (
              <option
                key={`${key} ${item.id}`}
                className="option"
                value={item.id}
              >
                {item.name_ru}
              </option>
            );
          })}
      </select>
    </label>
  );
}
