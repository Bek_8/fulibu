import React, { useContext, useEffect, useState } from "react";
import Select from "../Search/Select";
import MyContext from "../../context/MyContext";

const Searching = () => {
  const { apiService, language } = useContext(MyContext);

  const [categories, setCategories] = useState([]);
  const [types] = useState([
    { id: 1, name_ru: "Text" },
    { id: 2, name_ru: "Audio" },
  ]);
  const [rating] = useState([
    { id: 1, name_ru: "1" },
    { id: 2, name_ru: "2" },
    { id: 3, name_ru: "3" },
    { id: 4, name_ru: "4" },
  ]);
  useEffect(() => {
    apiService.getResources("/categories").then((value) => {
      if (value.statusCode === 200) {
        setCategories(value.data);
      }
    });
  }, []);
  return (
    <>
      <div className="searching">
        <label>
          <input
            type="text"
            placeholder="Введите название книги"
            className="searching_input"
            name="keyword"
          />
        </label>
        <button type="submit" className="searching_search">
          <div className="searching_search_svg">
            <img src={require("../../images/searching.svg")} alt="" />
          </div>
          <div className="searching_search_text">Найти</div>
        </button>
      </div>
      <div className="searching_second">
        <div className="searching_second_sub">
          <div className="searching_second_subtext">Жанры</div>
          <Select
            name="category_id"
            data={[{ id: 0, name_ru: "Все" }, ...categories]}
          />
        </div>
        <div className="searching_second_sub">
          <div className="searching_second_subtext">Дата выпуска</div>
          <input type="date" name="date" className="searching_second_input" />
        </div>
        <div className="searching_second_sub">
          <div className="searching_second_subtext">Тип</div>
          <Select name="type" data={[{ id: 0, name_ru: "Все" }, ...types]} />
        </div>
        <div className="searching_second_sub">
          <div className="searching_second_subtext">Рейтинг</div>
          <Select name="rating" data={[{ id: 0, name_ru: "Все" }, ...rating]} />
        </div>
      </div>
    </>
  );
};

export default Searching;
