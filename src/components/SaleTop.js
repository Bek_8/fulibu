import React from "react";
import TopBook from "./TopBook";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.scss";
import SwiperCore, { Autoplay } from "swiper";

SwiperCore.use([Autoplay]);

const SaleTop = ({ topBooks }) => {
  return (
    <>
      <Swiper
        loop={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        className="wrap top_wrap"
        slidesPerView={4}
        breakpoints={{
          1200: {
            slidesPerView: 3,
          },
          992: {
            slidesPerView: 2,
          },
          768: {
            slidesPerView: 2,
          },
          600: {
            slidesPerView: 1,
          },
          400: {
            slidesPerView: 1,
          },
        }}
      >
        {topBooks &&
          [...topBooks, ...topBooks].map((topBook, index) => {
            return (
              <SwiperSlide key={`${index} ${topBook.id}`}>
                <TopBook topBook={topBook} />
              </SwiperSlide>
            );
          })}
      </Swiper>
    </>
  );
};

export default SaleTop;
