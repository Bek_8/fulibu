import React, { useContext } from "react";
import { Link } from "react-router-dom";
import MyContext from "../context/MyContext";
import language from "../language/index";

const About = () => {
  const context = useContext(MyContext);
  const { lang } = context;
  return (
    <>
      <div className="fullscreen-wrap">
        <div className="about_text">
          <div className="container">
            <h1 className="about_text__title">{language[lang].notRead}</h1>
            <Link to="/search-page">
              <div className="about_text__more">
                <div className="about_text__button">{language[lang].more}</div>
                <button className="btn about_text__key">
                  <img src={require("../images/right.svg")} alt="" />
                </button>
              </div>
            </Link>
            <img
              className="about_text__book"
              src={require("../images/nav_book.png")}
              alt=""
            />
            <img
              className="about_text__768"
              src={require("../images/nav_book768.png")}
              alt=""
            />
            <img
              className="about_text__400"
              src={require("../images/nav_book400.png")}
              alt=""
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default About;
