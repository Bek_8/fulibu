import React from "react";
import { Link, withRouter } from "react-router-dom";
import MyContext from "../context/MyContext.js";

class Nav extends React.Component {
  static contextType = MyContext;
  constructor() {
    super();
  }
  state = {
    auth: false,
    slide: 0,
    lastScrollY: 0,
  };

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    const { lastScrollY } = this.state;
    const currentScrollY = window.scrollY;

    if (currentScrollY > lastScrollY) {
      this.setState({ slide: "-100px" });
    } else {
      this.setState({ slide: "0px" });
    }
    this.setState({ lastScrollY: currentScrollY });
  };
  render() {
    const { token } = this.context;
    return (
      <nav
        className={`nav`}
        style={{
          transform: `translate(0, ${this.state.slide})`,
          transition: "transform 90ms linear",
        }}
      >
        <ul>
          <li>
            <Link className="link nav_search" to="/search-page">
              <img src={require("../images/searchnav.svg")} alt="" />
            </Link>
          </li>
          <li className="nav_center">
            <Link to="/" className="link nav_logo">
              <img src={require("../images/logo-back.svg")} />
            </Link>
          </li>
          <li>
            {this.props.location.pathname === "/register" ? (
              <Link to="/">
                <img src={require("../images/regisster.svg")} alt="" />
              </Link>
            ) : (
              <Link
                className="link nav_profile"
                to={token ? "/profile" : "/auth"}
              >
                <img src={require("../images/profilenav.svg")} alt="" />
              </Link>
            )}
          </li>
        </ul>
      </nav>
    );
  }
}

export default withRouter(Nav);
