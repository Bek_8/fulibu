import React from "react";

const RegisterSvg = () => {
  return (
<>
    <div className="register_first_image">
      <img src={require("../../images/register.svg")} alt="" />
    </div>
    <div className="register_first_400">
    <img src={require("../../images/auth400.svg")} alt="" />
  </div>
  </>
  );
};

export default RegisterSvg;
