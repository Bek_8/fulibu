import React, { useState, useContext } from "react";
import RegisterSvg from "./RegisterSvg";
import MyContext from "../../context/MyContext";
import Loader from "../Loader";
import { withRouter } from "react-router-dom";

const RegisterAccept = ({ history }) => {
  const [code, setCodeNumber] = useState("");
  const [loading, setLoading] = useState(false);
  const { apiService, phone, setUserToken } = useContext(MyContext);
  const register = () => {
    if (code.length === 4) {
      setLoading(true);
      // setCodeNumber(code);
      const body = {
        phone,
        verify_code: code,
      };
      apiService
        .postData("/auth", null, body)
        .then((value) => {
          if (value.statusCode === 200) {
            setUserToken(value.data.token);
            history.push("/register");
          }
        })
        .catch((e) => setLoading(false));
    }
  };
  if (loading) return <Loader />;
  return (
    <div className="register">
      <div className="register_first">
        <RegisterSvg />
        <div className="register_first_text">ВХОД В АККАУНТ</div>
        <div className="register_second">
          <div className="register_second_text">Введите код подтверждения</div>
          <div className="register_second_input">
            <input
              type="number"
              value={code}
              onChange={(e) =>
                e.target.value.length < 5 && setCodeNumber(e.target.value)
              }
              placeholder="1234"
            />
          </div>
          <button onClick={() => register()} className="register_second_btn">
            Подтвердить
          </button>
        </div>
        <button className="register_third">Не получил код!</button>
      </div>
    </div>
  );
};
export default withRouter(RegisterAccept);
