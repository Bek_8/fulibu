import React, { useState, useContext } from "react";
import RegisterSvg from "./RegisterSvg";
import MyContext from "../../context/MyContext";
import Loader from "../Loader";
import { withRouter } from "react-router-dom";

const RegisterCreate = ({ history }) => {
  const [name, setName] = useState("");
  const [loading, setLoading] = useState(false);
  const { apiService, token } = useContext(MyContext);
  const referal = () => {
    if (name) {
      setLoading(true);
      setName(name);
      const body = {
        name: name,
      };
      apiService
        .updateData("/account", token, body)
        .then((value) => {
          if (value.statusCode) {
            history.push("/referal-link");
          }
        })
        .catch((e) => setLoading(false));
    }
  };
  if (loading) return <Loader />;

  const something = (event) => {
    if (event.keyCode === 9) {
      console.log("enter");
    }
  };

  return (
    <div className="register">
      <form onSubmit={(e) => something(e)} className="register_first">
        <RegisterSvg />
        <div className="register_first_text">СОЗДАНИЕ АККАУНТА</div>
        <div className="register_second">
          <div className="register_second_text">Введите ваше ИМЯ</div>
          <div className="register_second_input">
            <input
              value={name}
              onChange={(e) => e.target.value && setName(e.target.value)}
              type="text"
              placeholder="Abu"
            />
          </div>
          <button onClick={() => referal()} className="register_second_btn">
            Сохранить
          </button>
        </div>
      </form>
    </div>
  );
};

export default withRouter(RegisterCreate);
