import React, { useState, useContext } from "react";
import MyContext from "../../context/MyContext";
import Loader from "../Loader";
import RegisterSvg from "../Register /RegisterSvg";
import { withRouter } from "react-router-dom";

const RegisterMain = ({ history }) => {
  const [phone, setPhoneNumber] = useState("");
  const [loading, setLoading] = useState(false);
  const { apiService, setUserPhone, token } = useContext(MyContext);
  const confirm = () => {
    if (phone.length === 9) {
      setLoading(true);
      setUserPhone(phone);
      const body = {
        phone: phone,
      };
      apiService
        .postData("/verification", token, body)
        .then((value) => {
          if (value.statusCode === 200) {
            history.push("/confirm");
          }
        })
        .catch((e) => setLoading(false));
    }
  };
  if (loading) return <Loader />;

  const something = (event) => {
    if (event.keyCode === 9) {
      console.log("enter");
    }
  };

  return (
    <div className="register">
      <form onSubmit={(e) => something(e)} className="register_first">
        <RegisterSvg />
        <div className="register_first_text">ВХОД В АККАУНТ</div>
        <div className="register_second">
          <div className="register_second_text">
            Введите свой номер телефона
          </div>
          <div className="register_second_input">
            <input
              type="number"
              value={phone}
              onChange={(e) =>
                e.target.value.length < 10 && setPhoneNumber(e.target.value)
              }
              placeholder="1234567"
            />
          </div>
          <button
            onClick={() => confirm()}
            type="submit"
            className="register_second_btn"
          >
            Получить код
          </button>
        </div>
      </form>
    </div>
  );
};
export default withRouter(RegisterMain);
