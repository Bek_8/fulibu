import React, { useState } from "react";

const RegisterInput = () => {
  const [phone, setPhone] = useState("");
  return (
    <div className="register_second_input">
      <input
        type="number"
        value={phone}
        onChange={(e) => e.target.value.length < 10 && setPhone(e.target.value)}
        placeholder="1234567"
      />
    </div>
  );
};

export default RegisterInput;
