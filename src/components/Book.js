import React from "react";
import { Link } from "react-router-dom";
import StarRatings from "react-star-ratings";

const Book = ({ book }) => {
  const { name, author, price, rating, category, image } = book;
  return (
    <Link to={`/product/${book.id}`} className="book">
      <div className="book_title">{author.name}</div>
      <div className="book_image">
        <img src={image[0]} alt="" />
        <button className="book_image_text">Текст</button>
        <button className="book_image_audio">Аудио</button>
      </div>
      <div className="book_genree">
        <div className="book_genre">{name}</div>
        <div className="book_star">
          <StarRatings
            rating={parseInt(rating)}
            starRatedColor="#FE8D00"
            numberOfStars={5}
            starDimension="15px"
            starSpacing="2px"
            name="rating"
          />
        </div>
      </div>
      <div className="book_footer">
        <div className="footer_cost">{price}</div>
        <button className="btn footer_btn">
          <img src={require("../images/rightwhite.svg")} alt="" />
        </button>
      </div>
    </Link>
  );
};
export default Book;
