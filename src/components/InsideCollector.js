import React from "react";
import { Link } from "react-router-dom";
import StarRatings from "react-star-ratings";

const TopBook = ({ collectorBook }) => {
  return (
    <Link to={`/collection/${collectorBook.id}`} className="topbook blur">
      <div className="topbook_first">
        <div className="topbook_first_image">
          <img src={collectorBook.books[0].image} alt="" />
        </div>
        <div className="topbook_first_image1">
          <img src={collectorBook.books[0].image} alt="" />
        </div>
        <div className="topbook_first_text">
          <div className="text">
            <div className="text_title">{collectorBook.name}</div>
            <div className="text_author">{collectorBook.books[0].author.name}</div>
            <div className="text_star">
              <StarRatings
                rating={parseInt(collectorBook.rating)}
                starRatedColor="#FE8D00"
                numberOfStars={5}
                starDimension="15px"
                starSpacing="2px"
                name="rating"
              />
            </div>
            <div className="text_words">{collectorBook.description}</div>
          </div>
        </div>
      </div>
      <div className="topbook_second">
        <div className="topbook_second_select">
          <button className="select_text">Текст</button>
          <button className="select_audio">Аудио</button>
        </div>
        {/* <div className="topbook_second_price">{price}</div> */}
        <button className="btn topbook_second_button">
          <img src={require("../images/rightwhite.svg")} alt="" />
        </button>
      </div>
    </Link>
  );
};

export default TopBook;
