import React, { useEffect, useContext, useState } from "react";
import Recommend from "./Recommend";
import SaleTop from "./SaleTop";
import Collector from "./Collector";
import MyContext from "../context/MyContext";
import Loader from "../components/Loader";
import { Link, withRouter } from "react-router-dom";
import language from "../language/index";
import AboutCircle from "./AboutCircle";

const Content = () => {
  const context = useContext(MyContext);
  const { apiService, lang } = context;
  const [loading, setLoading] = useState(true);
  const [recommend, setRecommend] = useState([]);
  const [collector, setCollector] = useState([]);
  const [saleTop, setSaleTop] = useState([]);
  useEffect(() => {
    apiService
      .getResources("/main")
      .then((value) => {
        if (value.statusCode === 200) {
          console.log(value);
          setRecommend(value.newest);
          setCollector(value.collection);
          setSaleTop(value.hot);
        }
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  }, []);
  if (loading) return <Loader />;
  return (
    <>
      <div className="relative">
        <div className="container">
          <div className="recommendation">{language[lang].recommend}</div>
        </div>
        <div className="container">
          <Recommend books={recommend} />
        </div>
      </div>
      <Link to="/search-page">
        <div className="recommendation_btn">
          <div className="recommendation_more">
            <div className="recommendation_more_text">
              {language[lang].more}
            </div>
            <button className="recommendation_more_btn">
              <img src={require("../images/right.svg")} alt="" />
            </button>
          </div>
        </div>
      </Link>
      <div className="container">
        <div className="top">{language[lang].hot}</div>
      </div>
      <SaleTop topBooks={saleTop} />
      <div className="container">
        <div className="collector">{language[lang].collection}</div>
      </div>
      <Collector collectorBooks={collector} />
      <div className="container">
        <div className="top">{language[lang].new}</div>
      </div>
      <AboutCircle reversed />
      <div className="container">
        <Recommend books={recommend} />
      </div>
      <Link to="/search-page">
        <div className="recommendation_btn">
          <div className="recommendation_more">
            <div className="recommendation_more_text">
              {language[lang].more}
            </div>
            <button className="recommendation_more_btn">
              <img src={require("../images/right.svg")} alt="" />
            </button>
          </div>
        </div>
      </Link>
    </>
  );
};

export default Content;
