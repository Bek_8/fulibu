import React, { useContext, useState, useEffect } from "react";
import MyContext from "../../context/MyContext";
import Loader from "../Loader";

const getLevels = [1, 2, 3, 4, 5, 6];
const ProfileHeader = () => {
  const { apiService, token } = useContext(MyContext);
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    apiService
      .getResources("/user", token)
      .then((value) => {
        setUser(value);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  }, []);
  if (loading) return <Loader />;

  const getCurrentPercentage = 80;
  const getInnerFullPercentage = 100 / 6;

  return (
    <>
      <div className="profile">
        <div className="profile_title">
          <div className="profile_title_text">Выиграй свой автомобиль</div>
          <div className="profile_title_car">
            <img src={require("../../images/spark.svg")} alt="" />
          </div>
        </div>
        <div className="profile_subtitle">
          <div className="level">
            {getLevels.map((level) => {
              const levelFullPercentage = level * getInnerFullPercentage;
              const checker =
                levelFullPercentage <= getCurrentPercentage
                  ? 100
                  : getCurrentPercentage - levelFullPercentage;
              const getPercentage =
                checker === 100
                  ? 100
                  : Math.abs(checker) > getInnerFullPercentage
                  ? 0
                  : 100 - (Math.abs(checker) * 100) / getInnerFullPercentage;
              return (
                <div className={`level_${level} level-wrap `}>
                  <div className="inner">
                    <div
                      className="after"
                      style={{
                        width: getPercentage + "%",
                        background: getPercentage === 100 ? "green" : "orange",
                      }}
                    ></div>
                  </div>
                  <div className="text">
                    {level} Уровень {getPercentage}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="profile_info">
          <div className="profile_info_first">
            <div className="profile_info_greeting">
              <div className="greeting_text">Привет</div>
              <div className="greeting_name">{user.name}</div>
              <div className="greeting_text">!</div>
            </div>
            <div className="profile_info_friends">
              <div className="first_text">Количество друзей:</div>
              <div className="first_text">{user.friends.count}/4</div>
            </div>
            <div className="profile_info_shop">
              <div className="first_text">Количество покупок:</div>
              <div className="first_text">{user.friends.order_count}/5</div>
            </div>
            <div className="profile_info_sum">
              <div className="first_text">Сумма покупок:</div>
              <div className="first_text">{user.friends.order_sum}$</div>
            </div>
            <div className="profile_info_cashback">
              <div className="first_text_orange">Вознаграждение:</div>
              <div className="first_text_orange">{user.friends.reward}$</div>
            </div>
          </div>
          <div className="profile_info_second">
            <div className="second_item">
              <div className="second_item_title">Реферальная ссылка</div>
              <a className="second_item_link" target="_blank">
                "fulibu.uz/{user.uuid}"
              </a>
            </div>
            <div className="second_button">
              <img src={require("../../images/copy.svg")} alt="" />
              <button className="second_button_text">Скопировать</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileHeader;
