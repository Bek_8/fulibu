import React from "react";
import Select from "../Search/Select";

const ProfileMain = () => {
  return (
    <>
      <div className="container">
        <div className="library">Библиотeка</div>
        <Select />
      </div>
    </>
  );
};

export default ProfileMain;
