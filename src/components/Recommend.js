import React from "react";
import Book from "./Book";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import "swiper/swiper.scss";
SwiperCore.use([Autoplay]);

const Recommend = ({ books }) => {
  return (
    <>
      <Swiper
        loop={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        className="wrap recommendation-wrap"
        slidesPerView={4}
        breakpoints={{
          1200: {
            slidesPerView: 4,
          },
          768: {
            slidesPerView: 3,
          },
          400: {
            slidesPerView: 2,
          },
        }}
      >
        {books &&
          books.map((book, index) => {
            return (
              <SwiperSlide key={`${index} ${book.id}`}>
                <Book book={book} />
              </SwiperSlide>
            );
          })}
      </Swiper>
    </>
  );
};

export default Recommend;
