import React from "react";
import StarRatings from "react-star-ratings";

const TopBook = ({ topBook }) => {
  const { name, author, price, text, category, image, rating } = topBook;
  return (
    <div className="topbook">
      <div className="topbook_first">
        <div className="topbook_first_image">
          <img src={image[0]} alt="" />
        </div>
        <div className="topbook_first_image1">
          <img src={image[0]} alt="" />
        </div>
        <div className="topbook_first_text">
          <div className="text">
            <div className="text_title">{name}</div>
            <div className="text_author">{author.name}</div>
            <div className="text_star">
              <StarRatings
                rating={parseInt(rating)}
                starRatedColor="#FE8D00"
                numberOfStars={5}
                starDimension="15px"
                starSpacing="2px"
                name="rating"
              />
            </div>
            <div className="text_words">{text}</div>
          </div>
        </div>
      </div>
      <div className="topbook_second">
        <div className="topbook_second_select">
          <button className="select_text">Текст</button>
          <button className="select_audio">Аудио</button>
        </div>
        <div className="topbook_second_price">{price}</div>
        <button className="btn topbook_second_button">
          <img src={require("../images/rightwhite.svg")} alt="" />
        </button>
      </div>
    </div>
  );
};

export default TopBook;
