import React from "react";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <div className="footer_first">
          <Link to="/">
            <div className="footer_first_logo">
              <img src={require("../images/footerlogo.svg")} alt="" />
            </div>
          </Link>
          <div className="footer_first_400">
            <div className="footer_first_column">
              <div className="footer_first_title">FULIBU</div>
              <div className="footer_first_item">О компании</div>
              <div className="footer_first_item">Публичная оферта</div>
              <div className="footer_first_item">Служба поддержки</div>
              <div className="footer_first_item">Контакты</div>
            </div>
            <div className="footer_first_column">
              <div className="footer_first_title">Сотрудничество</div>
              <div className="footer_first_item">Издательствам</div>
              <div className="footer_first_item">Авторам</div>
              <div className="footer_first_item">Библиотекам</div>
              <div className="footer_first_item">Вебмастерам</div>
            </div>
          </div>
        </div>
        <div className="footer_second">
          <div className="footer_second_fulibu">© ООО «FULIBU»</div>
          <div className="footer_second_social">
            <div className="footer_second_social">
              <img src={require("../images/instagram.svg")} alt="" />
            </div>
            <div className="footer_second_social">
              <img src={require("../images/facebook.svg")} alt="" />
            </div>
            <div className="footer_second_social">
              <img src={require("../images/telegram.svg")} alt="" />
            </div>
          </div>
          <div className="footer_second_400">
            <div className="footer_second_fulibu2">© ООО «FULIBU»</div>
            <div className="footer_second_innovative">
              <div className="footer_second_item">Разработано командой </div>
              <a
                href="https://indev.uz"
                target="_blank"
                className="footer_second_team"
              >
                <div className="footer_second_logo">
                  <img src={require("../images/innovativelogo.svg")} alt="" />
                </div>
                <div className="footer_second_teamname">
                  Innovative Development Group
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
