import React from "react";

const AboutCircle = ({ reversed = false }) => {
  return (
    <div className={reversed && "reversed"}>
      <span className="about_circle_right"></span>
      <span className="about_circle_left"></span>
    </div>
  );
};

export default AboutCircle;
