class ApiService {
  _host = "https://admin.fulibu.uz";
  _apiBase = this._host + "/api";
  _headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  updateData = (url, token, form) => {
    let headers = this._headers;
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + url, {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => res.json());
  };
  // deleteMethod = (url, token) => {
  //   let headers = {};
  //   if (token) {
  //     headers = {
  //       Accept: "application/json",
  //       "Content-Type": "application/json",
  //       Authorization: `Bearer ${token}`,
  //     };
  //   }
  //   return fetch(this._apiBase + url, {
  //     method: "DELETE",
  //     headers: headers,
  //   })
  //     .then((res) => {
  //       return res.json();
  //     })
  //     .then((value) => {
  //       return value;
  //     });
  // };
  postData = (url, token, form) => {
    let headers = this._headers;
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + url, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => res.json());
  };
  getResources = (url, token = null) => {
    let headers = [];

    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }

    return fetch(this._apiBase + url, { headers }).then((res) => res.json());
  };
}
export default ApiService;
